﻿#include <iostream>

using namespace std;

class Animal  //вводим родительский класс
{
public:
	Animal() {}
	virtual void Voice() //создаем способный к перегрузке метод Void
	{
		cout << "arrrrghh!" << endl;
	}
};
class Coala : public Animal //добавляем три производных от Animal класса
{
public:
	Coala() {}
	void Voice() override //без override перегрузка все равно работает, непонятно, зачем он нужен
	{
		cout << "Unts unts" << endl;
	}
};

class Camal : public Animal
{
public:
	Camal() {}
	void Voice() override
	{
		cout << "Tfffu" << endl;
	}
};

class Human : public Animal
{
public:
	Human() {}
	void Voice() override
	{
		cout << "i don't understand" << endl;
	}
};

int main()
{
	Animal* CoalaHarry = new Coala; //создаем три объекта каждого класса
	Animal* CamalPumpkin = new Camal;
	Animal* HumanInga = new Human;

	Animal* pArr[3] = {}; //создаем массив и инициализируем его объектами классов животных
	pArr[0] = CoalaHarry;
	pArr[1] = CamalPumpkin;
	pArr[2] = HumanInga;

	for (int i = 0; i < 3; i++) //проходим циклом по массиву, вызывая метод Voice в каждом
	{
		pArr[i]->Voice();
	}

	Animal* Test = new Human; // тест - вызов функции из произвольного класса
	Test->Voice();
}